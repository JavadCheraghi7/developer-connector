const jwt = require("jsonwebtoken");
const config = require("config");

// this middleware validating token for us
module.exports = function (req, res, next) {
  // Get token from header
  const token = req.header("x-auth-token");

  // check if not token
  if (!token) {
    return res.status(401).json({ msg: "توکن وجود ندارد، احراز هویت رد شد" });
  }

  // Verify token
  try {
    const decoded = jwt.verify(token, config.get("jwtSecret"));

    req.user = decoded.user;
    next();

  } catch (err) {
    res.status(401).json({ msg: "توکن معتبر نیست" });
    
  }
};

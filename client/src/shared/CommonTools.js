export function numberMapEnFa(str) {
    let persianNumbers = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
  
    if (typeof str === "string") {
      str = str.replace(/0/g, persianNumbers[0]);
      str = str.replace(/1/g, persianNumbers[1]);
      str = str.replace(/2/g, persianNumbers[2]);
      str = str.replace(/3/g, persianNumbers[3]);
      str = str.replace(/4/g, persianNumbers[4]);
      str = str.replace(/5/g, persianNumbers[5]);
      str = str.replace(/6/g, persianNumbers[6]);
      str = str.replace(/7/g, persianNumbers[7]);
      str = str.replace(/8/g, persianNumbers[8]);
      str = str.replace(/9/g, persianNumbers[9]);
    }
    return str;
  }
  
  export function numberMapFaEn(str) {
  
    if (typeof str === "string") {
      str = str.replace(/۰/g, 0);
      str = str.replace(/۱/g, 1);
      str = str.replace(/۲/g, 2);
      str = str.replace(/۳/g, 3);
      str = str.replace(/۴/g, 4);
      str = str.replace(/۵/g, 5);
      str = str.replace(/۶/g, 6);
      str = str.replace(/۷/g, 7);
      str = str.replace(/۸/g, 8);
      str = str.replace(/۹/g, 9);
    }
    return str;
  }
import React from 'react';
import {Grid, Avatar, Typography, Button, IconButton, Badge} from "@material-ui/core"; 
import {FaThumbsUp, FaThumbsDown} from "react-icons/fa";
import {connect} from "react-redux";
import moment from "moment-jalaali";
import theme from "../../theme";
import {removeLike, addLike, deletePost} from "../../actions/post";
import {numberMapEnFa} from "../../shared/CommonTools";



const PostItem = ({auth, post:{_id, text, name, date, avatar, likes, comments, user}, removeLike, addLike, deletePost, showActions}) => {
    console.log(auth.user);
    return (
        <Grid container style={{boxShadow: '2px 2px 2px rgba(0, 0, 0, 0.2)', border: "0.7px solid #ccc", borderRadius: 16, marginTop: 24}}>

            <Grid item sm={3} xs={12} style={{padding: 16}}>
                <Avatar src={avatar} style={{width: 120, height: 120}} />
            </Grid>    

            <Grid item sm={9} xs={12} style={{padding: 16}}>
                <Typography variant="body1" color="textSecondary" style={{textAlign: "left", fontSize: 14}}>{moment(date).format("jYYYY/jMM/jDD")}</Typography>
                <Typography variant="h6">{name}</Typography>
                <Typography variant="body2" style={{marginTop: 48, marginBottom: 64}}>{text}</Typography>
                {
                    showActions && <>

                    <Button variant="contained" href={`/posts/${_id}`}>
                        {comments.length > 0 && (
                            <Badge style={{marginLeft: 16}} badgeContent={numberMapEnFa((comments.length).toString())} color="primary"></Badge>)
                        }
                        کامنت ها
                    </Button> 
                    <IconButton variant="contained" onClick={() => removeLike(_id)} style={{marginRight: 24}}><FaThumbsDown style={{fontSize: 16}} /></IconButton>
                    <IconButton variant="contained" onClick={() => addLike(_id)} style={{fontSize: 16, marginRight: 8, marginLeft: 8}}>
                        {likes.length > 0 && (
                            <Badge style={{fontSize: 14, marginLeft: 16}} badgeContent={numberMapEnFa((likes.length).toString())} color="primary"></Badge>)
                        }    
                        <FaThumbsUp />
                    </IconButton>
                    
                    {
                        !auth.loading && user === auth.user._id && (
                            <Button variant="contained" style={{background: theme.palette.funRed.main, float: "left", color: "#fff"}} onClick={() => deletePost(_id)}>حذف</Button>
                        )   
                    }

                    </>
                }
            </Grid>

        </Grid>
    )
};

PostItem.defaultProps = {
    showActions: true
}


const mapStateToProps = state =>({
    auth: state.auth
})

export default connect(mapStateToProps, {removeLike, addLike, deletePost})(PostItem);

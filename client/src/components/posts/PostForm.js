import React, {useState} from 'react';
import {Box, TextField, Button} from "@material-ui/core";
import {connect} from "react-redux";
import {addPost} from "../../actions/post";


const PostForm = ({addPost}) => {

    const [text, setText] = useState("");

    return (
        <form onSubmit={(e) =>{
            e.preventDefault();
            addPost({text});
            setText("");
        }}
        
        style={{marginBottom: 56}}
        >
            <TextField 
                variant="outlined"
                name="text"
                label="* متن خود را وارد کنید"
                multiline
                fullWidth
                value={text}
                rows={4}
                onChange={(e) => setText(e.target.value)}
            /><br/>

            <Button variant="contained" type="submit" color="secondary"style={{marginTop: 16}} >ارسال</Button>
        </form>
    )
}

export default connect(null, {addPost})(PostForm);

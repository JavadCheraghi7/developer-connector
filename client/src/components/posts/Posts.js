import React, {useEffect} from 'react';
import {Container, Card, CardContent, CardHeader, Typography} from "@material-ui/core"; 
import {getPosts} from "../../actions/post";
import {connect} from "react-redux";
import Spinner from "../layout/Spinner";
import PostItem from "./PostItem";
import PostForm from "./PostForm";



const Posts = ({getPosts, post: {posts, loading}}) => {

    useEffect(() =>{
        getPosts();

    }, [getPosts]);


    if(loading){
        return (<Spinner />);
    };

    return (
        <Container maxWidth={"md"}>
            <Card style={{marginTop: 24, marginBottom: 24, minHeight: "calc(100vh - 112px)"}}>
                <CardHeader
                    title={
                        <Typography variant="h3" color="primary">پست ها</Typography>
                    }                    

                    style={{background: "#f5f5f5", padding: 24, borderBottom: "1px solid #ccc"}}                
                />

                <CardContent>

                    <PostForm />

                    {
                        posts.map(post =>{
                            return (<PostItem key={post._id} post={post} />)
                        })
                    }
                </CardContent>    

            </Card>
        </Container>
    );
};

const mapStateToProps = state =>({
    post: state.post
})

export default connect(mapStateToProps, {getPosts})(Posts);

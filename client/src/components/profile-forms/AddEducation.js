import React, {useState} from 'react';
import {withRouter} from "react-router-dom";
import {Container, Card, TextField, CardHeader, CardContent, Typography,
         FormControlLabel, Checkbox, Button} from "@material-ui/core";
import {connect} from "react-redux";
import {addEducation} from "../../actions/profile";
import jMoment from "moment-jalaali";
import JalaliUtils from "@date-io/jalaali";
import {DatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";

jMoment.loadPersian({ dialect: "persian-modern", usePersianDigits: true });



const AddEducation = ({addEducation, history}) => {

    const [formData, setFormData] = useState({
        school: "",
        degree: "",
        fieldofstudy: "",
        current: false,
        description: ""
    });

    const [from, setFromDate] = useState(null);
    const [to, setToDate] = useState(null);
    const [toDataDisable, toggleDisable] = useState(false);

    const {school, degree, fieldofstudy, current, description} = formData;

    const handleChange = (e) =>{
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        });
    };

    const handleSendData = (e) =>{
        // e.preventDefault();
        addEducation({...formData, from, to}, history);
    };


    return (
        
        <Container maxWidth={"sm"} style={{display: "flex", alignItems: "center", justifyContent: "center", height: "calc(100vh - 64px)"}}> 
            <Card>
                <CardHeader
                    title={
                        <Typography variant="h4" color="primary">سابقه تحصیلی خود را وارد کنید</Typography>
                    }
                    style={{background: "#f5f5f5", padding: 24, borderBottom: "1px solid #ccc"}}                
                />
                    
                <CardContent style={{padding: 24}}>
                    <TextField
                        type="text"
                        variant="outlined"
                        label="* نام محل تحصیل"
                        name="school"
                        value={school}
                        onChange={(e) => handleChange(e)}
                        fullWidth
                        style={{marginTop: 16}}
                    >
                    </TextField>

                    <TextField
                        type="text"
                        variant="outlined"
                        label="* مقطع تحصیلی"
                        name="degree"
                        value={degree}
                        onChange={(e) =>handleChange(e)}
                        fullWidth
                        style={{marginTop: 24}}
                    >
                    </TextField>

                    <TextField
                        type="text"
                        variant="outlined"
                        label="* رشته تحصیلی"
                        name="fieldofstudy"
                        value={fieldofstudy}
                        onChange={(e) => handleChange(e)}
                        fullWidth
                        style={{marginTop: 24}}
                    >
                    </TextField>
                    
                    <MuiPickersUtilsProvider utils={JalaliUtils} locale="fa" >

                        <DatePicker
                            // variant="inline"
                            inputVariant="outlined"
                            style={{marginTop: 24 }}
                            label="* تاریخ شروع تحصیل"
                            clearable
                            okLabel="تأیید"
                            cancelLabel="لغو"
                            clearLabel="پاک کردن"
                            fullWidth
                            labelFunc={date => (date ? date.format("jDD/jMMMM/jYYYY") : "")}
                            value={from}
                            onChange={setFromDate}
                        />
                    </MuiPickersUtilsProvider>

                    <MuiPickersUtilsProvider utils={JalaliUtils} locale="fa" >

                        <DatePicker
                            // variant="inline"
                            inputVariant="outlined"
                            style={{ marginTop: 24 }}
                            label="تاریخ خاتمه تحصیل"
                            clearable
                            okLabel="تأیید"
                            cancelLabel="لغو"
                            clearLabel="پاک کردن"
                            fullWidth
                            labelFunc={date => (date ? date.format("jDD/jMMMM/jYYYY") : "")}
                            value={to}
                            onChange={setToDate}
                            disabled={toDataDisable ? "disabled" : ""}
                        />
                    </MuiPickersUtilsProvider>

                    <FormControlLabel
                        control={
                        <Checkbox
                            checked={current}
                            onChange={() => {
                                setFormData({...formData, current: !current});
                                toggleDisable(!toDataDisable);
                            }}
                            name="current"
                            color="primary"
                        />
                        }
                        label="در حال تحصیل"
                        style={{marginTop: 16}}
                    />
                    
                    <TextField
                        type="text"
                        variant="outlined"
                        label="توضیحات"
                        name="description"
                        value={description}
                        onChange={handleChange}
                        fullWidth
                        multiline
                        rows={6}
                        style={{marginTop: 16}}
                    >
                    </TextField>

                    <Button variant="contained" color="secondary" size="large" style={{marginTop: 24}} onClick={(e) => handleSendData(e)}>ارسال</Button>
                    <Button href="/dashboard" variant="contained" size="large" color="inherit" style={{marginTop: 24, marginRight: 16}} >بازگشت</Button>

                </CardContent>
                
            </Card>  
        </Container>                
    )
}

export default connect(null, {addEducation})(withRouter(AddEducation));

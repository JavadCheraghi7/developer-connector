import React, {useState} from 'react';
import {Container, Grid, Typography, Card, Button, CardHeader, CardContent, TextField, InputAdornment, Collapse} from "@material-ui/core";
import ChipInput from 'material-ui-chip-input';
import {FaTwitter, FaFacebook, FaYoutube, FaLinkedin, FaInstagram, FaUser } from "react-icons/fa"
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {createProfile} from "../../actions/profile";



const CreateProfile = ({createProfile, history}) => {
   
    const [formData, setFormData] = useState({
        company: "",       
        website: "",           
        location: "",          
        status: "",          
        skills: [],
        bio: "",
        githubusername: "",
        youtube: "",
        twitter: "",
        facebook: "",
        linkedin: "",
        instagram: ""
    });

    const [toggleDisplaySocial, setToggleDisplaySocial] = useState(false);

    const {
        company,       
        website,           
        location,          
        status,          
        skills,
        bio,
        githubusername,
        youtube,
        twitter,
        facebook,
        linkedin,
        instagram
    } = formData;


    const handleChange = (e) =>{
        setFormData({
            ...formData,
            [e.target.name] : e.target.value

        })
    };
    
    const handleAddChip = (chip) =>{
        return setFormData({...formData, skills: [...skills, chip]});
    };


    const handleDeleteChip = (chip, index) =>{
        return setFormData({...formData, skills: skills.filter(item => item !== chip)});
    };    
        
    console.log(skills);


    const handleSendData = (e) =>{
        // e.preventDefault();
        createProfile(formData, history);
    };


    return (
        <Container maxWidth={"lg"}> 
            <Card style={{marginTop: 24, marginBottom: 24, minHeight: "calc(100vh - 112px)"}}>
                <CardHeader
                    title={
                        <Typography variant="h4" color="primary">پروفایل خود را بسازید</Typography>
                    }

                    style={{background: "#f5f5f5", padding: 24, borderBottom: "1px solid #ccc"}}
                
                />
                    
                <CardContent style={{padding: 24}}>
                    <Typography variant="body1" style={{marginTop: 16, marginBottom: 16}}>
                        <FaUser style={{marginLeft: 4, transform: "translateY(4px)"}} /> با ایجاد پروفایل در میان دیگر کاربران دیده شوید. 
                    </Typography>

                    
                    <Grid container spacing={2}>
                        <Grid item lg={6}>
                            <TextField
                                variant="outlined"
                                select
                                label="* سطح فعالیت"
                                name="status"
                                value={status}
                                onChange={(e) =>handleChange(e)}
                                SelectProps={{
                                    native: true,
                                }}
                                style={{marginTop: 24, width: "100%"}}
                            >
                                <option value=""></option>
                                <option value="Junior" > تازه کار</option>
                                <option value="Senior" > ارشد</option>
                                <option value="Expert" > متخصص</option>                                
                                <option value="Teacher" > آموزگار</option>                                
                            </TextField>

                            <TextField 
                                variant="outlined" 
                                value={company}
                                size="medium"
                                label="نام شرکت"
                                name="company"
                                fullWidth
                                style={{marginTop: 24}}
                                onChange={(e) => handleChange(e)}
                            >
                            </TextField>

                            <TextField 
                                variant="outlined" 
                                value={website}
                                size="medium"
                                label="وبسایت"
                                name="website"
                                fullWidth
                                style={{marginTop: 24}}
                                onChange={(e) => handleChange(e)}
                            >
                            </TextField>
                        </Grid>

                        <Grid item lg={6}>                              

                            <TextField 
                                variant="outlined" 
                                value={location}
                                size="medium"
                                label="آدرس"
                                name="location"
                                fullWidth
                                style={{marginTop: 24}}
                                onChange={(e) => handleChange(e)}
                            >
                            </TextField>

                            <TextField 
                                variant="outlined" 
                                value={githubusername}
                                size="medium"
                                label="نام کاربری Github"
                                name="githubusername"
                                fullWidth
                                style={{marginTop: 24}}
                                onChange={(e) => handleChange(e)}
                            >
                            </TextField>
                        </Grid>

                        <Grid item xs={12}>

                            <ChipInput 
                                label="* مهارت ها" 
                                variant="outlined" 
                                style={{width: "100%", marginTop: 8}}
                                value={skills}
                                onAdd={(chip) => handleAddChip(chip)}
                                onDelete={(chip, index) => handleDeleteChip(chip, index)}                                
                            />
                            
                            <TextField 
                                variant="outlined" 
                                value={bio}
                                size="medium"
                                label="درباره من"
                                name="bio"
                                fullWidth
                                multiline
                                rows={5}
                                style={{marginTop: 24}}
                                onChange={(e) => handleChange(e)}
                            >
                            </TextField>
                        </Grid>

                        <Button variant="contained" color="inherit" onClick={() => setToggleDisplaySocial(!toggleDisplaySocial)} style={{marginTop: 48}}>
                            اضافه کردن آدرس شبکه های اجتماعی ( اختیاری )
                        </Button>

                        {/* {toggleDisplaySocial &&  */}
                        <Collapse in={toggleDisplaySocial} timeout={1000}>       
                            <Grid item xs={12}>
                                <TextField 
                                    variant="outlined" 
                                    value={twitter}
                                    size="medium"
                                    label="آدرس Twitter"
                                    name="twitter"
                                    fullWidth
                                    style={{marginTop: 38}}
                                    onChange={(e) => handleChange(e)}
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">
                                                        <FaTwitter style={{fontSize: 24, color: "skyblue"}} />
                                                    </InputAdornment>
                                    }}
                                >
                                </TextField>
                                
                                <TextField 
                                    variant="outlined" 
                                    value={facebook}
                                    size="medium"
                                    label="آدرس Facebook"
                                    name="facebook"
                                    fullWidth
                                    style={{marginTop: 24}}
                                    onChange={(e) => handleChange(e)}
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">
                                                        <FaFacebook style={{fontSize: 24, color: "#4867aa"}} />
                                                    </InputAdornment>
                                    }}
                                >
                                </TextField>

                                <TextField 
                                    variant="outlined" 
                                    value={youtube}
                                    size="medium"
                                    label="آدرس Youtube"
                                    name="youtube"
                                    fullWidth
                                    style={{marginTop: 24}}
                                    onChange={(e) => handleChange(e)}
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">
                                                        <FaYoutube style={{fontSize: 24, color: "red"}} />
                                                    </InputAdornment>
                                    }}
                                >
                                </TextField>

                                <TextField 
                                    variant="outlined" 
                                    value={linkedin}
                                    size="medium"
                                    label="آدرس Linkedin"
                                    name="linkedin"
                                    fullWidth
                                    style={{marginTop: 24}}
                                    onChange={(e) => handleChange(e)}
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">
                                                        <FaLinkedin style={{fontSize: 24, color: "#0077b5"}} />
                                                    </InputAdornment>
                                    }}
                                >
                                </TextField>

                                <TextField 
                                    variant="outlined" 
                                    value={instagram}
                                    size="medium"
                                    label="آدرس Instagram"
                                    name="instagram"
                                    fullWidth
                                    style={{marginTop: 24}}
                                    onChange={(e) => handleChange(e)}
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">
                                                        <FaInstagram style={{fontSize: 24, color: "#f24688"}} />
                                                    </InputAdornment>
                                    }}
                                >
                                </TextField>
                            </Grid>
                        </Collapse>    
                        {/* }     */}
                    </Grid>

                    <Button variant="contained" type="submit" size="large" color="secondary" style={{marginTop: 56}} onClick={(e) => handleSendData(e)} >ارسال</Button>
                    <Button href="/dashboard" variant="contained" size="large" color="inherit" style={{marginTop: 56, marginRight: 16}} >بازگشت</Button>
                     
                </CardContent>
            </Card>     
        </Container>                
    )
}

export default connect(null, {createProfile})(withRouter(CreateProfile));

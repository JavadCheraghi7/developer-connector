import React, {useState} from 'react';
import {withRouter} from "react-router-dom";
import {Container, Card, TextField, CardHeader, CardContent, Typography,
         FormControlLabel, Checkbox, Button} from "@material-ui/core";
import {connect} from "react-redux";
import {addExperience} from "../../actions/profile";
import jMoment from "moment-jalaali";
import JalaliUtils from "@date-io/jalaali";
import { DatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";

jMoment.loadPersian({ dialect: "persian-modern", usePersianDigits: true });



const AddExperience = ({addExperience, history}) => {

    const [formData, setFormData] = useState({
        company: "",
        title: "",
        location: "",
        current: false,
        description: ""
    });

    const [from, setFromDate] = useState(null);
    const [to, setToDate] = useState(null);
    const [toDataDisable, toggleDisable] = useState(false);

    const {company, title, location, current, description} = formData;

    const handleChange = (e) =>{
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        });
    }

    console.log(from)
    const handleSendData = (e) =>{
        // e.preventDefault();
        addExperience({...formData, from, to}, history);
    };


    return (
        
        <Container maxWidth={"sm"} style={{display: "flex", alignItems: "center", justifyContent: "center", height: "calc(100vh - 64px)"}}> 
            <Card>
                <CardHeader
                    title={
                        <Typography variant="h4" color="primary">سابقه کاری خود را وارد کنید</Typography>
                    }
                    style={{background: "#f5f5f5", padding: 24, borderBottom: "1px solid #ccc"}}                
                />
                    
                <CardContent style={{padding: 24}}>
                    <TextField
                        type="text"
                        variant="outlined"
                        label="* نام شرکت"
                        name="company"
                        value={company}
                        onChange={(e) => handleChange(e)}
                        fullWidth
                        style={{marginTop: 16}}
                    >
                    </TextField>

                    <TextField
                        type="text"
                        variant="outlined"
                        label="* عنوان شغل"
                        name="title"
                        value={title}
                        onChange={(e) =>handleChange(e)}
                        fullWidth
                        style={{marginTop: 24}}
                    >
                    </TextField>

                    <TextField
                        type="text"
                        variant="outlined"
                        label="آدرس شرکت"
                        name="location"
                        value={location}
                        onChange={(e) => handleChange(e)}
                        fullWidth
                        style={{marginTop: 24}}
                    >
                    </TextField>
                    
                    <MuiPickersUtilsProvider utils={JalaliUtils} locale="fa" >

                        <DatePicker
                            // variant="inline"
                            inputVariant="outlined"
                            style={{marginTop: 24 }}
                            label="* تاریخ شروع کار"
                            clearable
                            okLabel="تأیید"
                            cancelLabel="لغو"
                            clearLabel="پاک کردن"
                            fullWidth
                            labelFunc={date => (date ? date.format("jDD/jMMMM/jYYYY") : "")}
                            value={from}
                            onChange={setFromDate}
                        />
                    </MuiPickersUtilsProvider>

                    <MuiPickersUtilsProvider utils={JalaliUtils} locale="fa" >

                        <DatePicker
                            // variant="inline"
                            inputVariant="outlined"
                            style={{ marginTop: 24 }}
                            label="تاریخ خاتمه کار"
                            clearable
                            okLabel="تأیید"
                            cancelLabel="لغو"
                            clearLabel="پاک کردن"
                            fullWidth
                            labelFunc={date => (date ? date.format("jDD/jMMMM/jYYYY") : "")}
                            value={to}
                            onChange={setToDate}
                            disabled={toDataDisable ? "disabled" : ""}
                        />
                    </MuiPickersUtilsProvider>

                    <FormControlLabel
                        control={
                        <Checkbox
                            checked={current}
                            onChange={() => {
                                setFormData({...formData, current: !current});
                                toggleDisable(!toDataDisable);
                            }}
                            name="current"
                            color="primary"
                        />
                        }
                        label="هنوز مشغولم"
                        style={{marginTop: 16}}
                    />
                    
                    <TextField
                        type="text"
                        variant="outlined"
                        label="توضیحات"
                        name="description"
                        value={description}
                        onChange={handleChange}
                        fullWidth
                        multiline
                        rows={6}
                        style={{marginTop: 16}}
                    >
                    </TextField>

                    <Button variant="contained" color="secondary" size="large" style={{marginTop: 24}} onClick={(e) => handleSendData(e)}>ارسال</Button>
                    <Button href="/dashboard" variant="contained" size="large" color="inherit" style={{marginTop: 24, marginRight: 16}} >بازگشت</Button>

                </CardContent>
                
            </Card>  
        </Container>                
    )
}

export default connect(null, {addExperience})(withRouter(AddExperience));

import React, {useState} from 'react';
import {TextField, Button} from "@material-ui/core";
import {connect} from "react-redux";
import {addComment} from "../../actions/post";

const CommentForm = ({addComment, postId}) => {
    
    const [text, setText] = useState("");

    return (
        <form onSubmit={(e) =>{
            e.preventDefault();
            addComment(postId, {text});
            setText("");
        }}
        
        style={{marginBottom: 56, marginTop: 56}}
        >
            <TextField 
                variant="outlined"
                name="text"
                label="* متن خود را وارد کنید"
                multiline
                fullWidth
                value={text}
                rows={4}
                onChange={(e) => setText(e.target.value)}
            /><br/>

            <Button variant="contained" type="submit" color="secondary"style={{marginTop: 16}} >ارسال</Button>
        </form>
    )
}

export default connect(null, {addComment})(CommentForm)

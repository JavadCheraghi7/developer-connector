import React, {useEffect} from 'react';
import {Container, Card, CardContent, Button} from "@material-ui/core";
import {connect} from "react-redux";
import Spinner from "../layout/Spinner";
import {getPost} from "../../actions/post";
import PostItem from "../posts/PostItem";
import CommentForm from "./CommentForm";
import CommentItem from "./CommentItem";

const Post = ({getPost, post:{loading, post}, match}) => {

    useEffect(() =>{
        getPost(match.params.id);
    }, [getPost, match.params.id]);

    if(loading || post === null){
        return (<Spinner />);
    }

    return (
        <Container maxWidth={"md"} >
            <Card style={{marginTop: 24, marginBottom: 24, minHeight: "calc(100vh - 112px)"}}>
                <CardContent>
                    <Button variant="contained" href="/posts" color="inherit">پست ها</Button>
                    <PostItem  post={post} showActions={false} />
                    <CommentForm postId={post._id} />

                    {
                        post.comments.length > 0 && post.comments.map(comment =>{
                            return (<CommentItem key={comment._id} comment={comment} postId={post._id} />)
                        })
                    }
                </CardContent>
            </Card>    
        </Container>    
    )
};


const mapStateToProps = state =>({
    post: state.post
})

export default connect(mapStateToProps, {getPost})(Post);

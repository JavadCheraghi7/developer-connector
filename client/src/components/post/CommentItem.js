import React from 'react';
import {Grid, Avatar, Typography, Button, IconButton, Badge} from "@material-ui/core";
import moment from "moment-jalaali";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {removeComment} from "../../actions/post";
import theme from "../../theme";



const CommentItem = ({comment:{text, name, avatar, date, user, _id}, postId, auth, removeComment}) => {
    return (
        <Grid container style={{boxShadow: '2px 2px 2px rgba(0, 0, 0, 0.2)', border: "0.7px solid #ccc", borderRadius: 16, marginTop: 24}}>

        <Grid item sm={3} xs={12} style={{padding: 16}}>
            <Link to={`/profile/${user}`}><Avatar src={avatar} style={{width: 120, height: 120}} /></Link>
        </Grid>    

        <Grid item sm={9} xs={12} style={{padding: 16}}>
            <Typography variant="body1" color="textSecondary" style={{textAlign: "left", fontSize: 14}}>{moment(date).format("jYYYY/jMM/jDD")}</Typography>
            <Typography variant="h6" >{name}</Typography>
            <Typography variant="body2" style={{marginTop: 48, marginBottom: 64}}>{text}</Typography>           
                
            {
                !auth.loading && user === auth.user._id && (
                    <Button variant="contained" style={{background: theme.palette.funRed.main, float: "left", color: "#fff"}} onClick={() => removeComment(postId, _id)}>حذف</Button>
                )   
            }

        </Grid>

    </Grid>
    )
}


const mapStateToProps = state => ({
    auth: state.auth
})

export default connect(mapStateToProps, {removeComment})(CommentItem);

import React, {useEffect} from 'react';
import {Grid, Typography, Box, Button, Chip, Divider} from "@material-ui/core";
import {connect} from "react-redux";
import {getRepos} from "../../actions/profile";
import Spinner from "../layout/Spinner";


const ProfileGithub = ({getRepos, username, repos}) => {

    useEffect(() =>{
        getRepos(username);
    }, [getRepos, username]);

    if(repos === null){
        return (<Spinner />);
    }

    return (
        <Grid container style={{marginTop: 16}}>
            <Grid item xs={12} style={{background: "#eee", padding: 24, borderRadius: 16, boxShadow: "2px 2px 2px rgba(0, 0, 0, 0.2)"}}>
                <Typography variant="h6" style={{fontWeight: "bold", color: "#444"}}>پروژه های Github</Typography>
                <Divider style={{height: 0.5, marginTop: 16}} />

                <Box style={{marginTop: 24}}>
                    {repos.map((repo, index) =>{
                        
                        return (
                            <>
                            <Grid container  style={{marginTop: 16}} spacing={2}>
                                <Grid item md={6} sm={12} xs={12}>
                                    <Button href={repo.html_url} target="_blank" style={{fontSize: 15, fontWeight: "bold", color: "#555"}}>{repo.name}</Button>
                                    <Typography  variant="body2" style={{marginTop: 16}}>{repo.description}</Typography>
                                </Grid>  

                                <Grid item md={6} sm={12} xs={12} style={{textAlign: "left"}}>
                                    <Chip
                                        label={`${repo.stargazers_count} :امتیاز `}
                                        color="primary"
                                    />
                                    <Chip
                                        label={`${repo.watchers_count} :بازدید `}
                                        color="primary"
                                        style={{marginLeft: 8, marginRight: 8}}
                                    />
                                    <Chip
                                        label={`${repo.forks_count} :مشارکت `}
                                        color="primary"
                                    />
                                </Grid>
                            </Grid>

                            {repos.length === (index + 1) ? null : (<Divider style={{height: 0.5, marginTop: 16}} />)}

                            </>
                        )
                    })}
                </Box>    
            </Grid>            
        </Grid>
    )
}


const mapStateToProps = state =>({
    repos: state.profile.repos
})

export default connect(mapStateToProps, {getRepos})(ProfileGithub);

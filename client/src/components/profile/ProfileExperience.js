import React from 'react';
import {Typography, Box, Divider} from "@material-ui/core";
import moment from "moment-jalaali";



const ProfileExperience = ({experience: {title, company, location, current, from, to, description}, inx}) => {
    console.log(description, "dfdfdfd");
    return (
        <Box>
            <Typography variant="subtitle1" style={{marginTop: 24, marginBottom: 16}}>{company}</Typography>
            <Typography variant="body2">                
                <span>{moment(from).format("jYYYY/jMM/jDD")}</span> - {" "}
                <span>{to ? moment(to).format("jYYYY/jMM/jDD") : "هنوز مشغولم"}</span>                
            </Typography>
            <Typography variant="body2" style={{marginTop: 16, marginBottom: 16}}>پست {title}</Typography>
            <Typography variant="body2">{description}</Typography>
            {inx ? null : (<Divider style={{marginTop: 24, height: 0.5}} />)}
        </Box>
    )
}

export default ProfileExperience;

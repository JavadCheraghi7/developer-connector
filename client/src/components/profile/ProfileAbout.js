import React from 'react';
import {Box, Typography, Button} from "@material-ui/core";
import {FaCheck} from "react-icons/fa";
import theme from "../../theme";


const ProfileAbout = ({profile: {bio, skills, user:{name}}}) => {

    return (
        <>
        <Box style={{marginTop: 16, textAlign: "center", background: "#eee", padding: 16, borderRadius: 16, boxShadow: "2px 2px 2px rgba(0, 0, 0, 0.2)"}}>
            <Typography variant="h6" style={{fontWeight: "bold", color: "#444"}}>بیوگرافی {name.split(" ")[0]}</Typography>
            <Typography variant="body1" style={{marginTop: 16}}>{bio}</Typography>
        </Box>

        <Box style={{marginTop: 16, textAlign: "center", background: "#eee", padding: 16, borderRadius: 16, boxShadow: "2px 2px 2px rgba(0, 0, 0, 0.2)"}}>
            <Typography variant="h6" style={{fontWeight: "bold", color: "#444"}}>مهارت ها</Typography>
            <Typography variant="body1" style={{marginTop: 16}}>
                {
                    skills.map((skill, index) =>{
                        return (<Button key={index}>{skill}<FaCheck style={{color: theme.palette.secondary.main, marginRight: 4}} /></Button>)
                    })
                }
            </Typography>
        </Box>
        </>
    )
}

export default ProfileAbout

import React from 'react';
import {makeStyles} from "@material-ui/styles";
import {Box, Avatar, Typography, IconButton} from "@material-ui/core";
import {FaGlobe, FaTwitter, FaFacebook, FaYoutube, FaLinkedin, FaInstagram} from "react-icons/fa";


const useStyles = makeStyles((theme) =>({

    topProfile: {
        background: "#eee", 
        padding: 32, 
        marginTop: 32, 
        display: "flex", 
        justifyContent: "center", 
        flexDirection: "column", 
        alignItems: "center", 
        borderRadius: 16,
        overflow: "hidden",
        position: "relative",
        zIndex: 100,
        boxShadow: "-3px 3px 3px rgba(0, 0, 0, 0.2)",


        "&::before": {
            content: '""',
            clipPath: "polygon(0 0, 3% 0, 65% 100%, 0 100%)",
            background: theme.palette.secondary.main,
            width: "100%",
            height: "100%",
            position: "absolute",
            zIndex: -1,
        }
    }

}));


const ProfileTop = ({profile: {company, location, website, social, status, user: {avatar, name}}}) => {

    const classes = useStyles();
    
    return (
        <Box className={classes.topProfile}>
            <Avatar src={avatar} style={{width: 150, height: 150}} />
            <Typography variant="h4" style={{marginTop: 16}}>{name}</Typography>
            <Typography variant="subtitle1" style={{marginTop: 16}}>{status} در {company && company}</Typography>
            <Typography variant="body2" style={{marginTop: 16}}>{location && location}</Typography>
            <Box style={{marginTop: 8}}>
                {website && (<IconButton href={website} target="_blank"><FaGlobe/></IconButton>)}
                {social && social.twitter && (<IconButton href={social.twitter} target="_blank" ><FaTwitter/></IconButton>)}
                {social && social.facebook && (<IconButton href={social.facebook} target="_blank"><FaFacebook/></IconButton>)}
                {social && social.youtube && (<IconButton href={social.youtube} target="_blank"><FaYoutube/></IconButton>)}
                {social && social.linkedin && (<IconButton href={social.linkedin} target="_blank"><FaLinkedin/></IconButton>)}
                {social && social.instagram && (<IconButton href={social.instagram} target="_blank"><FaInstagram/></IconButton>)}
            </Box>
        </Box>
    )
}

export default ProfileTop;

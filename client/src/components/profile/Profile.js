import React, {useEffect} from 'react';
import {Container, Grid, Paper, Box, Typography, Button, Divider} from "@material-ui/core";
import {connect} from "react-redux";
import Spinner from "../layout/Spinner";
import {getProfileById} from "../../actions/profile";
import ProfileTop from "./ProfileTop";
import ProfileAbout from "./ProfileAbout";
import ProfileExperience from "./ProfileExperience";
import ProfileEducation from "./ProfileEducation";
import ProfileGithub from "./ProfileGithub";



const Profile = ({getProfileById, profile: {loading, profile}, auth, match}) => {
    console.log(match.params.id);

    useEffect(() =>{
        getProfileById(match.params.id);
    }, [getProfileById, match.params.id]);


    if(loading || profile === null){
        return (<Spinner />);
    };


    return (
        <Container maxWidth={"lg"} >
            <Paper style={{marginTop: 24, marginBottom: 24, minHeight: "calc(100vh - 112px)", padding: 24}}>
                <Button variant="contained" color="inherit" href="/profiles" >کاربران</Button>
                { auth.isAuthenticated && auth.loading === false && auth.user._id === profile.user._id &&
                    (<Button variant="contained" color="primary" href="/edit-profile" style={{marginRight: 16}} >ویرایش پروفایل</Button>)
                }

                <ProfileTop profile={profile} />
                <ProfileAbout profile={profile} />
                                
                
                <Grid container spacing={2} style={{marginTop: 8}}>
                    <Grid item md={7} xs={12} >
                        <Box style={{background: "#eee", borderRadius: 16, padding: 16, boxShadow: "2px 2px 2px rgba(0, 0, 0, 0.2)"}}>
                            <Typography variant="subtitle1" style={{textAlign: "right", fontSize: 20, fontWeight: "bold", color: "#444"}}>سابقه شغلی</Typography>
                            <Divider style={{height: 0.5, marginTop: 16}} />
                            {
                                profile.experience.length > 0 ? profile.experience.map((experience, index) =>{
                                    const inx = profile.experience.length === (index + 1);
                                    return (<ProfileExperience key={experience._id} experience={experience} inx={inx} />)
                                }) : (<Typography variant="subtitle1" style={{marginTop: 24}}>سابقه شغلی وارد نشده</Typography>)
                            }
                        </Box>    
                    </Grid>  

                  
                    <Grid item md={5} xs={12} >
                        <Box style={{background: "#eee", borderRadius: 16, padding: 16, boxShadow: "2px 2px 2px rgba(0, 0, 0, 0.2)"}}>
                        <Typography variant="subtitle1" style={{textAlign: "right", fontSize: 20, fontWeight: "bold", color: "#444"}}>سابقه تحصیلی</Typography>
                        <Divider style={{height: 0.5, marginTop: 16}} />

                            {
                                profile.education.length > 0 ? profile.education.map((education, index) =>{
                                    const inx = profile.education.length === (index + 1);
                                    return (<ProfileEducation key={education._id} education={education} inx={inx} />)
                                }) : (<Typography variant="subtitle1" style={{marginTop: 24}}>سابقه تحصیلی وارد نشده</Typography>)
                            }
                        </Box>    
                    </Grid>    
                </Grid>  

                {/* گیت هاب درخواست جداگانه دارد */}
                {profile.githubusername && (<ProfileGithub username={profile.githubusername} />)} 

            </Paper>
        </Container>
    )
}


const mapStateToProps = state =>({
    profile: state.profile,
    auth: state.auth
})


export default connect(mapStateToProps, {getProfileById})(Profile);

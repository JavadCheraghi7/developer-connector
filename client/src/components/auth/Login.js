import React, {useState} from 'react';
import {Grid, Card, TextField, Button, Typography, IconButton, InputAdornment} from "@material-ui/core";
import { FaEye, FaEyeSlash } from "react-icons/fa";
import {Link, Redirect} from "react-router-dom";
import {connect} from "react-redux";
import {login} from "../../actions/auth";



const Login = ({login, auth: {user}}) => {

    const [showPassword, setShowPassword] = useState(false);
    const [userData, setUserData] = useState({
        email: "",
        password: ""
    });

    const {email, password} = userData;

    // onChange
    const handleChange = (e) =>{
        setUserData({...userData, [e.target.name] : e.target.value});
    }

    // submit 
    const handleSubmit = async (e) =>{
        e.preventDefault();
        login(email, password);
    }

    if(user){
        return <Redirect to="/dashboard" />
    }

    return (
       <Grid container style={{height: "calc(100vh - 64px)", justifyContent: "center", alignItems: "center", textAlign: "center"}}>
           <Grid item xs={3}>
               <Card style={{padding: 24}}>
                    <Typography variant="h4" style={{marginTop: 24}}>فرم ورود</Typography>
                    <form onSubmit={(e) => handleSubmit(e)}>
                        <TextField 
                            variant="outlined" 
                            value={email}
                            name="email"
                            fullWidth
                            placeholder="ایمیل"
                            style={{marginTop: 32}}
                            onChange={(e) => handleChange(e)}
                        >
                        </TextField>

                        <TextField 
                            type={showPassword ? "text" : "password"}
                            variant="outlined" 
                            value={password}
                            name="password"
                            fullWidth
                            placeholder="رمز عبور"
                            style={{marginTop: 16}}
                            onChange={(e) => handleChange(e)}
                            InputProps={{
                                endAdornment: <InputAdornment position="end">
                                                    <IconButton
                                                        aria-label="toggle password visibility"
                                                        onMouseDown={() => {
                                                            setShowPassword(true);
                                                        }}
                                                        onMouseUp={() => {
                                                            setShowPassword(false);
                                                        }}
                                                        edge="end"
                                                    >
                                                        {showPassword ? <FaEye /> : <FaEyeSlash />}
                                                    </IconButton>
                                                </InputAdornment>
                            }}
                                
                        >
                            
                        </TextField>

                        <Button variant="contained" type="submit" value="login" color="secondary" size="large" fullWidth style={{marginTop: 16}}>ورود</Button>
                        <Typography variant="body2" style={{marginTop: 24, textAlign: "right"}}>
                            حساب کاربری ندارید ؟ <Link to="/register" >ثبت نام</Link>
                        </Typography>
                    </form>
                </Card>
           </Grid>
       </Grid>
    )
}


const mapStateToProps = state =>({
    auth: state.auth
});

export default connect(mapStateToProps, {login})(Login);


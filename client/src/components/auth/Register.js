import React, {useState} from 'react';
import {Grid, Card, TextField, Button, Typography, IconButton, InputAdornment} from "@material-ui/core";
import { FaEye, FaEyeSlash } from "react-icons/fa";
import {Link, Redirect} from "react-router-dom";
import {connect} from "react-redux";
import {setAlert} from "../../actions/alert";
import {register} from "../../actions/auth";



const Register = ({setAlert, register, auth:{user}}) => {

    const [showPassword, setShowPassword] = useState(false);
    const [userData, setUserData] = useState({
        name: "",
        email: "",
        password: "",
        password2: ""
    });

    const {name, email, password, password2} = userData;

    // onChange
    const handleChange = (e) =>{
        setUserData({...userData, [e.target.name] : e.target.value});
    }

    // submit 
    const handleSubmit = async (e) =>{
        e.preventDefault();
        if(password !== password2){
            setAlert("رمز عبور مانند هم نیستند", "danger");
        }
        else{
            register(name, email, password);
        }
    }


    if(user){
        return <Redirect to="/dashboard" />
    }

    return (
       <Grid container style={{height: "calc(100vh - 64px)", justifyContent: "center", alignItems: "center", textAlign: "center"}}>
           <Grid item lg={3} md={4} sm={6} xs={12} style={{marginRight: 8, marginLeft: 8}}>
               <Card style={{padding: 24}}>
                    <Typography variant="h4" style={{marginTop: 24}}>فرم ثبت نام</Typography>
                    <Typography variant="subtitle1" color="textSecondary" style={{marginTop: 16}}>حساب کاربری بسازید</Typography>
                    <form onSubmit={(e) => handleSubmit(e)}>
                        <TextField 
                            variant="outlined" 
                            value={name}
                            name="name"
                            fullWidth
                            placeholder="نام کاربری"
                            style={{marginTop: 24}}
                            onChange={(e) => handleChange(e)}
                        >
                        </TextField>
                        <TextField 
                            variant="outlined" 
                            value={email}
                            name="email"
                            fullWidth
                            placeholder="ایمیل"
                            style={{marginTop: 16}}
                            onChange={(e) => handleChange(e)}
                        >
                        </TextField>

                        <TextField 
                            type={showPassword ? "text" : "password"}
                            variant="outlined" 
                            value={password}
                            name="password"
                            fullWidth
                            placeholder="رمز عبور"
                            style={{marginTop: 16}}
                            onChange={(e) => handleChange(e)}
                            InputProps={{
                                endAdornment: <InputAdornment position="end">
                                                    <IconButton
                                                        aria-label="toggle password visibility"
                                                        onMouseDown={() => {
                                                            setShowPassword(true);
                                                        }}
                                                        onMouseUp={() => {
                                                            setShowPassword(false);
                                                        }}
                                                        edge="end"
                                                    >
                                                        {showPassword ? <FaEye /> : <FaEyeSlash />}
                                                    </IconButton>
                                                </InputAdornment>
                            }}
                                
                        >
                            
                        </TextField>

                        <TextField 
                            type="password"
                            variant="outlined" 
                            value={password2}
                            name="password2"
                            fullWidth
                            placeholder="تکرار رمز عبور"
                            style={{marginTop: 16}}
                            onChange={(e) => handleChange(e)}
                        >
                        </TextField>

                        <Button variant="contained" type="submit" value="register" color="secondary" size="large" fullWidth style={{marginTop: 16}}>ثبت نام</Button>
                        <Typography variant="body2" style={{marginTop: 24, textAlign: "right"}}>
                            حساب کاربری دارید ؟ <Link to="/login" >ورود</Link>
                        </Typography>
                    </form>
                </Card>
           </Grid>
       </Grid>
    )
}


const mapStateToProps = state =>({
    auth: state.auth
});

export default connect(mapStateToProps, {setAlert, register})(Register);

import React from 'react';
import {Card, CardHeader, CardContent, Avatar, Typography, Button, List, ListItem, ListItemText} from "@material-ui/core";



const ProfileItem = ({profile: {user: {_id, name, avatar}, company, skills, status, location}}) => {

    return (
        <Card elevation={3} style={{marginTop: 32, display: "flex"}} >
            <CardHeader
                avatar={
                    <Avatar src={avatar} style={{width: 150, height: 150}} />
                }

                title={
                    <Typography variant="h6">{name}</Typography>
                }
                subheader={
                    <>
                        <Typography variant="body1" style={{marginTop: 16}} >{status} {company && <span>در {company}</span>}</Typography>
                        <Typography variant="body2" style={{marginTop: 16}} >{location && location}</Typography>
                        <Button variant="contained" color="inherit" href={`/profile/${_id}`} style={{marginTop: 24}}>مشاهده پروفایل</Button>
                    </>
                }

                style={{flex: 7}}
            />

            <CardContent style={{flex: 5, textAlign: "center"}}>
                <List component="nav" style={{textAlign: "center"}}>
                    {
                        skills.slice(0, 4).map((skill, index) =>{
                            return(<ListItem button key={index} style={{textAlign: "center"}}>
                                <ListItemText primary={skill} />
                            </ListItem>)
                        })
                    }
                </List>
            </CardContent>
        </Card>
    )
}

export default ProfileItem;

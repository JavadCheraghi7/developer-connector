import React, {useEffect} from 'react';
import {Container, Card} from "@material-ui/core";
import {connect} from "react-redux";
import {getProfiles} from "../../actions/profile";
import Spinner from "../layout/Spinner";
import ProfileItem from "./ProfileItem";

const Profiles = ({profile:{loading, profiles}, getProfiles}) => {

    useEffect(() =>{
        getProfiles();
    }, [getProfiles]);


    if(loading || profiles.length === 0){
        return <Spinner />
    }
    
    return (
        <Container maxWidth={"lg"} style={{padding: 24, minHeight: "calc(100vh - 64px)"}}>
            <Card style={{padding: "0 32px 32px 32px"}}>
                {
                    profiles.map(profile =>{
                        return <ProfileItem key={profile._id} profile={profile} />
                    })
                }
            </Card>
        </Container>
    )
    
}


const mapStateToProps = state =>({
    profile: state.profile
})


export default connect(mapStateToProps, {getProfiles})(Profiles);

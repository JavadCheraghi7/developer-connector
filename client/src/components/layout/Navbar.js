import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {AppBar, Toolbar, Typography, Button} from '@material-ui/core';
import { FaCode, FaSignOutAlt, FaUser } from "react-icons/fa";
import {connect} from "react-redux";
import {logout} from "../../actions/auth";


const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
  },

  title: {
    flexGrow: 1,
    textAlign: "right"
  },

  navItem: {
    textTransform: "capitalize", 
    fontSize: 18, 
    marginRight: 8, 
    marginLeft: 8
  }

}));

const ButtonAppBar = ({auth: {loading, isAuthenticated}, logout}) =>{
  const classes = useStyles();

  const authLinks = (
    <>
      <Button onClick={logout} color="inherit" href="#!" className={classes.navItem} >خروج <FaSignOutAlt style={{marginRight: 4}} /></Button>
      <Button color="inherit" href="/dashboard" className={classes.navItem} >داشبورد <FaUser style={{marginRight: 4}} /></Button>
      <Button color="inherit" href="/posts" className={classes.navItem} >پست ها </Button>
      <Button color="inherit" href="/profiles" className={classes.navItem} >توسعه دهندگان </Button>
    </>
  );

  const guestLinks = (
    <>
      <Button variant="contained" color="secondary" href="/login" style={{marginLeft: 8}}>ورود</Button>
      <Button color="inherit" href="/register" className={classes.navItem} >ثبت نام</Button>
      <Button color="inherit" href="/profiles" className={classes.navItem} >توسعه دهندگان</Button>
    </>  
  )

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          {!loading && (<>{isAuthenticated ? authLinks : guestLinks}</>)}

          <Typography variant="h5" className={classes.title}>
            <a href="/" style={{textDecoration: "none", color: "#fff"}} >
              <FaCode style={{transform: "translateY(5px)"}} /> نشست برنامه نویسان
            </a>
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}

const mapStateToProps = state =>({
  auth: state.auth
})


export default connect(mapStateToProps, {logout})(ButtonAppBar);
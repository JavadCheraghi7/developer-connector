import React from 'react';
import {makeStyles} from "@material-ui/core/styles"
import {Box, Typography, Button} from "@material-ui/core";
import landing from "../../assets/img/landing.jpg";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";



const useStyles = makeStyles(() =>({

    landing: {
        width: "100%",
        height: "calc(100vh - 64px)",
        backgroundImage: `url(${landing})`,
        backgroundSize: "cover",
        backgroundPosition: "center center",
        backgroundRepeat: "no-repeat",
        filter: "opacity(0.2)",
        position: "relative"
    },

    landingText: {
        position: "absolute", 
        top: "50%", 
        left: "50%", 
        transform: "translate(-50%, -50%)", 
        color: "#fff",
        textAlign: "center"
    }

}))

const Landing = ({isAuthenticated}) => {

    const classes = useStyles();

    if(isAuthenticated){
        return <Redirect to="/dashboard" />
    }

    return (
        <>
            <Box className={classes.landing}></Box>
            <Box className={classes.landingText}>
                <Typography variant="h1" style={{fontSize: 82}}>نشست برنامه نویسان</Typography>
                <Typography variant="h6" style={{marginTop: 24, marginBottom: 40}}>
                    حساب کاربری بسازید, و با به اشتراک گذاشتن پست های خود کمک بگیرید
                </Typography>

                <Button variant="contained" color="secondary" size="large" href="/login" style={{marginLeft: 8}}>ورود</Button>
                <Button variant="contained" color="primary" size="large" href="/register" style={{marginRight: 8}}>ثبت نام</Button>
            </Box>
        </>
    )
};

const mapStateToProps = state =>({
    isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps)(Landing);

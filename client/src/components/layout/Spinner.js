import React from "react";
import spinner from "../../assets/svg/spinner.svg";

const Spinner = () => {
  return (
    <div style={{width: "100%", height: "calc(100vh - 64px)", display: "flex", alignItems: "center", justifyContent: "center", position: "absolute"}}>
      <img
        src={spinner}
        alt="spinner"
        style={{ width: 150, display: "block" }}
      />
    </div>  
  );
};

export default Spinner;

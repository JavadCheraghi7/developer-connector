import React from 'react';
import Alert from "@material-ui/lab/Alert";
import {connect} from "react-redux";

const Alerts = ({alerts, removeAlert}) => (
    
     alerts !== null && alerts.length > 0 && alerts.map(alert =>{
        return (<div style={{display: "flex", justifyContent: "center", alignItems: "center", marginTop: 16}}>
                    <Alert key={alert.id} severity={alert.alertType === "danger" ? "error" : "success"}>
                        {alert.msg}
                    </Alert>
                </div>
                )
        })           
         
)

const mapStateToProps = state =>({
    alerts: state.alert
})

export default connect(mapStateToProps)(Alerts);



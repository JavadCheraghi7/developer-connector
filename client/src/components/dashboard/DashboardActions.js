import React from 'react';
import {Button} from "@material-ui/core";
import {FaUserGraduate, FaUserCircle, FaUserTie} from "react-icons/fa";

const DashboardActions = () => {
    return (
       <div>
           <Button variant="contained" color="inherit" href="/add-education" >افزودن تحصیلات <FaUserGraduate  style={{marginRight: 8}} /></Button>
           <Button variant="contained" color="inherit" href="/add-experience" style={{marginRight: 16}} >افزودن سابقه شغلی <FaUserTie style={{marginRight: 8}} /></Button>
           <Button variant="contained" color="inherit" href="/edit-profile" style={{marginRight: 16}}>ویرایش پروفایل <FaUserCircle style={{marginRight: 8}} /></Button>
       </div>
    )
}

export default DashboardActions;

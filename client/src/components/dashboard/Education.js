import React from 'react';
import moment from "moment-jalaali";
import { withStyles } from '@material-ui/core/styles';
import {Paper, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Button, Typography} from "@material-ui/core";
import {connect} from "react-redux";
import theme from '../../theme';
import {deleteEducation} from "../../actions/profile";

moment.loadPersian({ dialect: "persian-modern", usePersianDigits: true });

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: "#777",
      color: theme.palette.common.white,
      fontSize: 17
    },
    body: {
      fontSize: 15,
    }

  }))(TableCell);
  
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      }
    },
  }))(TableRow);


const Education = ({education, deleteEducation}) => {
    return (
    <TableContainer component={Paper} style={{marginTop: 58}}>
        <Typography variant="h6" style={{marginBottom: 16}}>سابقه تحصیلی</Typography>
        <Table aria-label="customized table">
            <TableHead>
                <TableRow>
                    <StyledTableCell>نام محل تحصیل</StyledTableCell>
                    <StyledTableCell align="center">مقطع تحصیلی</StyledTableCell>
                    <StyledTableCell align="center">رشته تحصیلی</StyledTableCell>
                    <StyledTableCell align="center">مدت تحصیل</StyledTableCell>
                    <StyledTableCell align="right"></StyledTableCell>
                </TableRow>
            </TableHead>

            <TableBody>
                {education.map((edu) => (
                    <StyledTableRow key={edu._id}>
                        <StyledTableCell component="th" scope="row">
                            {edu.school}
                        </StyledTableCell>

                        <StyledTableCell align="center">{edu.degree}</StyledTableCell>

                        <StyledTableCell align="center">{edu.fieldofstudy}</StyledTableCell>

                        <StyledTableCell align="center">
                        {
                            <>
                                <span>{moment(edu.from).format("jYYYY/jMM/jDD")}</span> - {" "}
                                <span>{edu.to === null ? "در حال تحصیل" : moment(edu.to).format("jYYYY/jMM/jDD")}</span>
                            </>
                        }
                        </StyledTableCell>

                        <StyledTableCell align="right">
                            <Button variant="contained" style={{background: theme.palette.funRed.main, color: "#fff"}} onClick={() => deleteEducation(edu._id)}>حذف</Button>
                        </StyledTableCell>
                        
                    </StyledTableRow>
                ))}
            </TableBody>
        </Table>
    </TableContainer>
    )
}

export default connect(null, {deleteEducation})(Education);

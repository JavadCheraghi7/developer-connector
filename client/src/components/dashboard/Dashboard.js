import React, {useEffect} from 'react';
import {Container, Typography, Card, Button, CardHeader, CardContent} from "@material-ui/core";
import {connect} from "react-redux";
import Spinner from "../layout/Spinner";
import {getCurrentProfile, deleteAccount} from "../../actions/profile";
import DashboardActions from "./DashboardActions";
import Experience from "./Experience";
import Education from "./Education";
import theme from "../../theme";



const Dashboard = ({getCurrentProfile, deleteAccount, auth: {user}, profile: {loading, profile}}) => {

    useEffect(() =>{
        getCurrentProfile();
    }, [getCurrentProfile, user]);

    return loading && profile === null ? (<Spinner />) : (<>
    <Container maxWidth={"lg"}> 
        <Card style={{marginTop: 24, marginBottom: 24, minHeight: "calc(100vh - 112px)"}}>
            <CardHeader
                title={
                    <Typography variant="h2" color="primary">داشبورد</Typography>
                }

                subheader={
                    <Typography variant="subtitle1" style={{marginTop: 32, color: "#111", fontSize: 18}}>
                        {user && user.name} <span style={{color: "#666", marginRight: 2}}>عزیز خوش آمدی</span>
                    </Typography>
                }

                style={{background: "#f5f5f5", padding: 24, borderBottom: "1px solid #ccc"}}
            
            />
                
            <CardContent style={{padding: 24}}>
                
                {profile !== null ? 
                    (<>
                        <DashboardActions/>
                        <Experience experience={profile.experience} />
                        <Education education={profile.education} />

                        <Button variant="contained" style={{background: theme.palette.funRed.main, color: "#fff", marginTop: 56}} onClick={() => deleteAccount()}>
                            حذف حساب کاربری
                        </Button>

                    </>) : 
                    (<Typography variant="body1">
                        <Button variant='outlined' color="default" href="/create-profile">
                            شما پروفایلی ایجاد نکردید, لطفا پروفایل خود را بسازید
                        </Button>
                    </Typography>)}          
                
            </CardContent>


        </Card>
    </Container>

    </>)
};

const mapStateToProps = state =>({
    auth: state.auth,
    profile: state.profile
});

export default connect(mapStateToProps, {getCurrentProfile, deleteAccount})(Dashboard);

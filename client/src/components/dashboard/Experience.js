import React from 'react';
import moment from "moment-jalaali";
import { withStyles } from '@material-ui/core/styles';
import {Paper, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Button, Typography} from "@material-ui/core";
import theme from '../../theme';
import {connect} from "react-redux";
import {deleteExperience} from "../../actions/profile";


moment.loadPersian({ dialect: "persian-modern", usePersianDigits: true });

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: "#777",
      color: theme.palette.common.white,
      fontSize: 17
    },
    body: {
      fontSize: 15,
    }

  }))(TableCell);
  
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      }
    },
  }))(TableRow);


const Experience = ({experience, deleteExperience}) => {
    return (
    <TableContainer component={Paper} style={{marginTop: 64}}>
        <Typography variant="h6" style={{marginBottom: 16}}>سابقه شغلی</Typography>
        <Table aria-label="customized table">
            <TableHead>
                <TableRow>
                    <StyledTableCell>نام شرکت</StyledTableCell>
                    <StyledTableCell align="center">عنوان شغل</StyledTableCell>
                    <StyledTableCell align="center">مدت کار</StyledTableCell>
                    <StyledTableCell align="right"></StyledTableCell>
                </TableRow>
            </TableHead>

            <TableBody>
                {experience.map((exp, index) => (
                    <StyledTableRow key={exp._id}>
                        <StyledTableCell component="th" scope="row">
                            {exp.company}
                        </StyledTableCell>

                        <StyledTableCell align="center">{exp.title}</StyledTableCell>

                        <StyledTableCell align="center">
                        {
                            <>
                                <span>{moment(exp.from).format("jYYYY/jMM/jDD")}</span> - {" "}
                                <span>{exp.to === null ? "هنوز مشغولم" : moment(exp.to).format("jYYYY/jMM/jDD")}</span>
                            </>
                        }
                        </StyledTableCell>

                        <StyledTableCell align="right">
                            <Button variant="contained" style={{background: theme.palette.funRed.main, color: "#fff"}} onClick={() => deleteExperience(exp._id)}>حذف</Button>
                        </StyledTableCell>
                        
                    </StyledTableRow>
                ))}
            </TableBody>
        </Table>
    </TableContainer>
    )
}

export default connect(null, {deleteExperience})(Experience)

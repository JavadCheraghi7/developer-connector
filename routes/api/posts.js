const express = require("express");
const router = express.Router();
const { check, validationResult } = require("express-validator");
const auth = require("../../middleware/auth");

const User = require("../../models/User");
const Profile = require("../../models/Profile");
const Post = require("../../models/Post");

// route    POST api/posts
// desc     Create a post
// access   Private
router.post(
  "/",
  [auth, [check("text", "ضروری است").not().isEmpty()]],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const user = await User.findById(req.user.id).select("-password");
      const newPost = new Post({
        text: req.body.text,
        name: user.name,
        avatar: user.avatar,
        user: req.user.id,
      });

      const post = await newPost.save();
      res.json(post);
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server Error");
    }
  }
);

// route    Get api/posts
// desc     get all posts
// access   Private
router.get("/", auth, async (req, res) => {
  try {
    const posts = await Post.find().sort({ date: -1 });
    res.json(posts);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

// route    Get api/posts/:id
// desc     get post by ID
// access   Private
router.get("/:id", auth, async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);
    if (!post) {
      return res.status(404).json({ msg: "پست مورد نظر یافت نشد" });
    }
    res.json(post);
  } catch (err) {
    console.error(err.message);
    if (err.kind == "ObjectId") {
      return res.status(404).json({ msg: "پست مورد نظر یافت نشد" });
    }
    res.status(500).send("Server Error");
  }
});

// route    DELETE api/posts/:id
// desc     Delete a post
// access   Private
router.delete("/:id", auth, async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);

    if (!post) {
      return res.status(404).json({ msg: "پست مورد نظر پیدا نشد" });
    }
    // Check user
    if (post.user.toString() !== req.user.id) {
      return res.status(401).json({ msg: "کاربر احراز هویت نشده" });
    }
    await post.remove();
    res.json({ msg: "پست شما با موفقیت پاک شد" });
  } catch (err) {
    console.error(err.message);
    if (err.kind == "ObjectId") {
      return res.status(404).json({ msg: "پست مورد نظر پیدا نشد" });
    }
    res.status(500).send("Server Error");
  }
});


// route    PUT api/posts/like/:id
// desc     like a post
// access   Private
router.put('/like/:id', auth, async(req, res) =>{
  try {
    const post = await Post.findById(req.params.id);
    if(post.likes.filter(like => like.user.toString() === req.user.id).length > 0){
      return res.status(400).json({msg: "شما این پست را قبلا پسندیده اید"})
    }
    post.likes.unshift({user: req.user.id});
    await post.save();
    res.json(post.likes);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});


// route    PUT api/posts/unlike/:id
// desc     unlike a post
// access   Private
router.put('/unlike/:id', auth, async(req, res) =>{
  try {
    const post = await Post.findById(req.params.id);
    if(post.likes.filter(like => like.user.toString() === req.user.id).length === 0){
      return res.status(400).json({msg: "شما این پست را نپسندیده اید"})
    }
    const removeIndex = post.likes.map(like => like.user.toString()).indexOf(req.user.id);
    console.log(removeIndex);
    post.likes.splice(removeIndex, 1);
    await post.save();
    res.json(post.likes);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});


// route    POST api/posts/comment/:id
// desc     comment on a post
// access   Private
router.post(
  "/comment/:id",
  [auth, [check("text", "ضروری است").not().isEmpty()]],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const user = await User.findById(req.user.id).select("-password");
      const post = await Post.findById(req.params.id);

      const newComment = {
        text: req.body.text,
        name: user.name,
        avatar: user.avatar,
        user: req.user.id,
      };

      post.comments.unshift(newComment);

      await post.save();
      res.json(post.comments);

    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server Error");
    }
  }
);


// route    POST api/posts/comment/:id/:comment_id
// desc     comment on a post
// access   Private
router.delete("/comment/:id/:comment_id", auth, async(req, res) =>{
    try {
      const post = await Post.findById(req.params.id);

      // pull of comments
      const comment = post.comments.find(comment => comment.id === req.params.comment_id); 

      // not exist comment
      if(!comment){
        return res.status(404).json({msg: "Comment not exist!"});
      }

      // check user
      if(comment.user.toString() !== req.user.id){
        return res.status(401).json({msg: "User not Authorized"});
      };

      const commentIndex = post.comments.map(comment => comment.user.toString()).indexOf(req.user.id);
      post.comments.splice(commentIndex, 1);

      await post.save();

      res.json(post.comments);

    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server Error");
    }
  })

module.exports = router;
